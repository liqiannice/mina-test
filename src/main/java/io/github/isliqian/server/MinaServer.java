package io.github.isliqian.server;

import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.LineDelimiter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;

/**
 * Hello world!
 *
 */
public class MinaServer
{   //mina端口号
    static int PORT = 7080;
    //开发服务端的NIO
    static IoAcceptor acceptor = null;
    public static void main( String[] args )  {
        try {
        //实例化NioSocketAcceptor
        acceptor  = new NioSocketAcceptor();
        //设置过滤器，主要完成协议的编码解码
        acceptor.getFilterChain().addLast("liqian",new ProtocolCodecFilter(
                //指定编码解码器
                new TextLineCodecFactory(Charset.forName("UTF-8"),
                        LineDelimiter.WINDOWS.getValue(),
                        LineDelimiter.WINDOWS.getValue())
        ));
        acceptor.getFilterChain().addFirst("filter",new MyServiceFilter());
        //设置读缓冲区的大小
        acceptor.getSessionConfig().setReadBufferSize(1024);
        //设置空闲时间，十秒钟
        acceptor.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE,10);
        //设置Handle
        acceptor.setHandler(new MyServerHandler());
        //绑定端口号
        acceptor.bind(new InetSocketAddress(PORT));
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println( "Server Start 7080..." );
    }
}
