package io.github.isliqian.client;

import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.LineDelimiter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;

public class MinaClient {

    private static String host = "127.0.0.1";
    private static int port = 7080;

    public static void main(String[] args){
        IoSession session=null;
        //IO
        IoConnector connector = new NioSocketConnector();
        //设置超时时间
        connector.setConnectTimeout(3000);
        //设置过滤器,主要完成编码工作
        connector.getFilterChain().addLast("liqian",new ProtocolCodecFilter(
                //指定编码解码器
                new TextLineCodecFactory(Charset.forName("UTF-8"),
                        LineDelimiter.WINDOWS.getValue(),
                        LineDelimiter.WINDOWS.getValue())
        ));
        connector.getFilterChain().addFirst("filter",new MyClientFilter());
        //设置业务方法
        connector.setHandler(new MyClientHandler());
        //设置会话连接
        ConnectFuture future=connector.connect(new InetSocketAddress(host,port));
        future.awaitUninterruptibly(); //等待连接
        session = future.getSession();//获取session
        session.write("你好,世界");
        //设置关闭
        session.getCloseFuture().awaitUninterruptibly();//等待关闭连接
        connector.dispose();
    }
}
