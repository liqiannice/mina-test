package io.github.isliqian.client;


import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

import java.util.Date;

public class MyClientHandler extends IoHandlerAdapter {


    @Override
    public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
        System.out.println("连接异常...");
    }

    @Override
    public void messageSent(IoSession session, Object message) throws Exception {

        System.out.println("发送消息..."+message.toString());

    }

    @Override
    public void sessionClosed(IoSession session) throws Exception {
        System.out.println("连接关闭...");
    }

    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
        //读的数据为msg
        String msg = (String) message;
        System.out.println("客户端收到数据:"+msg);
//        if (msg.equals("exit")){
//            //关闭连接
//            session.close();
//        }
//        //发送一个时间
//        Date date = new Date();
//        session.write(date);
    }

    @Override
    public void sessionCreated(IoSession session) throws Exception {
        System.out.println("连接创建...");
    }

    @Override
    public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
        System.out.println("空闲状态...");
    }

    @Override
    public void sessionOpened(IoSession session) throws Exception {
        System.out.println("打开连接...");
    }
}
