My-Mina Study
----
### 体系结构介绍
1. mina在应用程序中处于什么样的地位
> 主要屏蔽了网络通信的一些细节，对socket进行封装，并且是NIO的一个实现架构，可以帮助我们快速的开发网络通信，常常用于游戏的开发，中间件等服务端程序。
2. IOService接口
> 用于描述我们的客户端和服务端接口，其子类是 **connect** 和 **acceptor** ,分别用于描述我们的客户端和服务端，IOproceser多线程环境来处理我们的连接请求流程。ioFilter提供数据的过滤工作：包括编解码，日志等信息的过滤，Handler 就是我们的业务对象，自定义的handler需要实现IOHandlerAcceptor。
3. IOConnector->IOProcessor->IOFilter->Handler  
4. IOAcceptor->IOProcessor->IOFilter->Handler  
5. 大致类图结构

``` 
                                IOService
                    IOConnector             IOAcceptor
                 NioSocketConnector     NioSocketAcceptor   
           IOSession:描述的是客户端和服务端连接的描述,常常用于接收发送数据
       
```  
            
#### 长短连接

1. 长连接:通信双方长期的保持一个连接状态不断开，比如QQ,当我们登陆qq的时候，我们就去连接我们的QQ服务器,一旦建立连接后，就不断开,除非发生异常,这样的方式就是长连接。长连接比较耗费资源。
2. 短链接:通信双方不是保持一个长期连接的状态,比如Http协议,当客户端发起了http请求的时候,服务器处理http请求,当服务器处理完成后,返回客户端数据后就断开连接,对于下次的连接请求需要重新发起,这种是我们常使用的方式。

#### IOService

1. IOService:实现了对网络通信的客户端和服务端之间的抽象，用户描述客户端的子接口IOConnector，用户描述服务端的子接口IOAcceptor。
2. 作用:IOService可以管理我们的网络通信的客户端和服务器，并且可以管理连接双方的会话session，同样可以添加过滤器。
3. 类结构:通过扩展子接口和抽象的子类达到扩展的目的。

``` 
                        IOService                     
                    abstractIOService
                IOConnector     IOAcceptor  
       abstractIOConnector      abstractIOAccptor
       NioSocketIOConnector         NioSocketAcceptor
      
       
```  

4. IOService常用API

> 1. getFilterChain() 获得过滤器
> 2. setHandler(IoHandler handler) 设置真正的业务
> 3. getSessionConfig() 得到我们的会话的配置信息
> 4. dispose() 在完成关闭连接的时候，所调用的方法

5. IOConnector常用API
> 1. connect(SocketAddress remoteAddress) 主要用户发起一个连接请求
> 2. setConnectTimeout(int connectTimeout) 连接超时的设置

6. IOAcceptor常用API
> 1. bind(SocketAddress localAddress) 绑定端口
> 2. getLocalAddress() 获得本地ip地址

7. NioSocketAcceptor常用API
> 1. accept(IoProcessor<NioSession> processor,ServerSocketChannel handler) 接收一个连接
> 2. open(SocketAddress localAddress) 打开一个socketchannel
> 3. select() 获得我们的选择器

8. NioSocketIOConnector常用API
> 1. connect(SocketChannel handle,SocketAddress remoteAddress) 用于描述连接请求
> 2. register(SocketChannel handle,AbstractPollingIoConnect.ConnectRequest request) 注册我们的IO事件
> 3. select(int timeout) 返回选择器

#### IOFilter

1. IOFilter:对应用程序和网络这块的传输，就是二进制数据和对象之间的相互转化，有相应的解码和编码器。这也是我们过滤器的一种，我们对过滤器还可以做日志，消息确认等功能。
2. 作用:是在我们的应用层和业务层之间的过滤层
3. 完成自定义过滤器
> 就是在往handler处理之前，需要调用相应的过滤器进行过滤。
> 1.  Client: 业务handler之前会调用我们的过滤器
> 2.  Server: 同样在我们接收到数据的时候，和发送数据也调用了我们的过滤器，然后才叫给我们的handler
 
 
#### IOSession

1. IOSession:主要描述我们的网络通信双方所建立的连接之间描述
2. 作用: 可以完成对于连接的一些管理，可以发送或者读取数据，并且可以设置我们会话的上下文信息
3. IOSessionConfig: 提供我们对连接的配置信息的描述，比如读缓冲区的设置等等
4. 作用: 设置读写缓冲区的一些信息，读和写的空闲事件，以及设置读写超时信息。
5. IOSession API
> 1. getAttribute(Object key) 根据key获得设置的上下文属性
> 2. setAttribute(Object key,Object value) 设置上下文属性
> 3. removeAttribute(Object key) 移除上下文属性
> 4. write(Object message) 发送数据
> 5. read() 读取数据
6. IOSessionConfig API
> 1. getBothIdleTime() 获得读写通用的空闲时间
> 2. setIdleTime(IdleStatus status,int idleTime) 设置我们读或者写的空闲时间
> 3. setReadBufferSize(int readBufferSize) 设置读缓冲区的大小
> 4. setWriteTimeout(int writeTimeout) 设置我们的写超时时间

#### IOProcessor

1. Processor: 是以NIO为基础实现的多线程的方式来完成读写工作
2. 作用: 是为我们的filter读写原始数据的多线程环境，如果mina不去实现的话，我们自己来实现Nio的话，需要自己写一个非阻塞读写的多线程环境
3. 配置Processor的多线程环境
> 1. 通过NioSocketAcceptor(int processorCount) 构造函数可以指定多线程的个数
> 2. 通过NioSocketConnector(int processorConut) 构造函数也可以指定多线程的个数

#### IOBuffer

1. IOBuffer是基于Java中的NioBuffer做了一个封装，用户操作缓冲区中的数据，包括基本数据类型以及字节数据和一些对象，其本质就是一个可动扩展的byte数组。
2. 索引属性:
> 1. Capacity: 代表当前缓冲区的大小 
> 2. Position: 理解成当前读写位置，也可以理解成下一个可读数据单位位置，Position <= Capacity 的时候可以完成数据的读写操作。
> 3. Limit: 就是下一个不可以被读写缓冲区单元的位置,Limit <= Capacity
3. 常用API
> 1. static allocate(int capacity) 已指定的大小开辟缓冲区空间
> 2. setAutoExpand(boolean autoExpand) 可以设置是否支持动态的扩展
> 3. putShort(int index,short value)
> 4. putString(CharSequence val,CharseEncoder encoder)
> 5. putInt(int value) 等等方法实现让缓冲区放入数据,PutXXX()
> 6. flip(): 就是让我们缓冲区的数据放入流中,一般在发送数据之前调用。
> 7. hasRemaining(): 缓冲区中是否有数据: boolean 是关于position<=limit=true,否则返回false.
> 8. reset(): 实现清空数据
> 9. clear(): 实现数据的覆盖，position=0；重新开始读取我们缓冲区的数据。

#### Mina自定义编解码器
 
1. 自定义的编解码工厂: 要实现编解码工厂就要实现**ProtocolCodecFactory**这个接口
2. 实现自定义的编解码器:
> 1. 实现自定义的解码器:实现ProtocolDecoder接口
> 2. 实现自定义的编码器:实现ProtocolEncoder接口
3. 根据自定义的编解码工厂获得我们的编解码对象
4. 为什么要使用自定义的编码器
> 因为在实际中往往不是通过一个字符串就可以传输所有的信息，我们传输的是自定义的协议包，并且能在应用程序和网络通信中存在对象和二进制流之间转化关系。所以我们需要结合业务编写自定义的编解码器。
5. 常用的自定义协议的方法
> 1. 定长的方式: Aa,bb,cc,ok,no等这样的通信方式
> 2. 定界符: helloworld|wachmen|....|... 通过特殊的字符来区别消息，这样的方式会出现粘包，半包等现象。带来了不正确消息，这样就应该丢弃数据。
> 3. 自定义协议包,包头:数据包的版本号，以及整个数据包(包头+包体)长度，包体:实际数据

